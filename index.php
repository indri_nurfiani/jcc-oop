<?php 

require_once('Animal.php');
require_once('Frog.php');
require_once('Ape.php');

$sheep = new Animal("shaun");
echo "Name : " . $sheep->name;
echo "<br>";
echo "legs : " . $sheep->legs;
echo "<br>";
echo "cold blooded : " . $sheep->cold_blooded;
echo "<br><br>";


$frog = new Frog("buduk");
echo "Name : " . $frog->name;
echo "<br>";
echo "legs : " . $frog->legs;
echo "<br>";
echo "cold blooded : " . $frog->cold_blooded;
echo "<br>";
echo "Jump : " . $frog->jump;
echo "<br><br>";

$ape = new Ape("kera sakti");
echo "Name : " . $ape->name;
echo "<br>";
echo "legs : " . $ape->legs;
echo "<br>";
echo "cold blooded : " . $ape->cold_blooded;
echo "<br>";
echo "Yell : " . $ape->yell;
echo "<br><br>";

?>